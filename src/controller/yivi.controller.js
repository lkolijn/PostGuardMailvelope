import mvelo from '../lib/lib-mvelo';
import * as l10n from '../lib/l10n';
import * as yiviCache from '../modules/yiviCache';
import {PromiseQueue, getHash, MvError} from '../lib/util';
import * as prefs from '../modules/prefs';
import {SubController} from './sub.controller';
import {getKey} from '../modules/postguardModel';
import {KEY_SORTS} from '../lib/constants';

export default class YiviController extends SubController {
  constructor(port) {
    if (port) {
      throw new Error('Do not instantiate YiviController with a port');
    }
    super(null);
    this.persistent = true;
    this.mainType = 'yiviDialog';
    this.id = getHash();
    this.queue = new PromiseQueue();
    this.yiviPopup = null;
    this.receivedPortMsg = false;
    this.options = null;
    this.resolve = null;
    this.reject = null;
    // register event handlers
    this.on('yivi-dialog-init', this.onYiviDialogInit);
    this.on('yivi-dialog-ok', this.onOk);
  }

  onYiviDialogInit() {
    this.ports.yiviDialog.emit('set-init-data', {
      sort: this.options.sort,
      con: this.options.con,
      heading: this.options.heading,
      title: this.options.title,
      qrPrefix: this.options.qrPrefix,
      hints: this.options.hints,
      senderId: this.options.senderId,
      timestamp: this.options.timestamp,
      cache: prefs.prefs.security.password_cache,
    });
  }

  onOk(msg) {
    Promise.resolve()
    .then(() => {
      this.options.jwt = msg.jwt;
      if (msg.cache != prefs.prefs.security.yivi_cache) {
        // update yivi cache status
        return prefs.update({security: {yivi_cache: msg.cache}});
      }
    })
    .then(() => {
      if (msg.cache) {
        yiviCache.set(this.options);
      }
    })
    .then(() => {
      this.receivedPortMsg = true;
      this.closePopup();
      this.resolve(msg.key);
    });
  }

  onCancel() {
    this.receivedPortMsg = true;
    this.closePopup();
    this.reject(new MvError(l10n.get('yivi_dialog_cancel'), 'YIVI_DIALOG_CANCEL'));
  }

  closePopup() {
    if (this.yiviPopup) {
      this.yiviPopup.close();
      this.yiviPopup = null;
    }
  }

  async requestKey(options) {
    const result = await this.queue.push(this, 'getKey', [options]);
    return result;
  }

  /**
   * @param {Object} options
   * @param {openpgp.key.Key} options.key - key to unlock
   * @param {String} [options.reason] - optional explanation for password dialog
   * @param {Boolean} [options.openPopup=true] - password popup required (false if dialog appears integrated)
   * @param {Function} [options.beforePasswordRequest] - called before password entry required
   * @param {String} [options.password] - password to unlock key
   * @param {Boolean} [options.noCache] - bypass cache
   * @return {Promise<Object, Error>} - resolves with unlocked key and password {key: openpgp.key.Key, password: String}
   */
  async getKey(options) {
    this.options = options;
    if (typeof options.reason == 'undefined') {
      this.options.reason = '';
    }
    if (typeof this.options.openPopup == 'undefined') {
      this.options.openPopup = true;
    }
    const cacheEntry = await yiviCache.get(this.options.con);
    if (cacheEntry && !options.noCache) {
      let keyrequest;
      if (this.options.sort == KEY_SORTS.Signing) {
        const pubSignId = this.options.con;
        const privSignId = [];
        keyrequest = {pubSignId, privSignId};
      }
      const key = getKey(cacheEntry, this.options.sort, this.options.timestamp, keyrequest);
      return Promise.resolve(key);
    } else {
      return new Promise((resolve, reject) => {
        if (this.options.beforeYiviRequest) {
          this.options.beforeYiviRequest(this.id);
        }
        if (this.options.openPopup) {
          setTimeout(
            () => mvelo.windows.openPopup(`components/yivi-popup/yiviDialog.html?id=${this.id}`, {width: 620, height: 700})
            .then(popup => {
              this.receivedPortMsg = false;
              this.yiviPopup = popup;
              popup.addRemoveListener(() => {
                if (!this.receivedPortMsg) {
                  this.yiviPopup = null;
                  this.onCancel();
                }
              });
            }), 50);
        }
        this.resolve = resolve;
        this.reject = reject;
      });
    }
  }
}
