import {PKG_URL, PG_CLIENT_HEADER, KEY_SORTS} from '../lib/constants';
import * as yivi from '@privacybydesign/yivi-frontend';
import {MvError} from '../lib/util';
import * as l10n from '../lib/l10n';
import {EMAIL_ATTRIBUTE_TYPE} from '../lib/constants';
import * as yiviCache from './yiviCache';

const mod_promise = import('@e4a/pg-wasm');

l10n.register(['yivi_recipient_unknown']);

export async function init() {
  yiviCache.init();
}

export function createYiviWeb(
  con,
  qr_prefix
) {
  const yivi_web = yivi.newWeb({
    debugging: false,
    element: '#yivi-web-form',
    language: 'en',
    translations: {
      header: qr_prefix
    },
    state: {
      serverSentEvents: false,
      polling: {
        endpoint: 'status',
        interval: 500,
        startState: 'INITIALIZED',
      },
    },
    session: {
      url: PKG_URL,
      start: {
        url: o => `${o.url}/v2/request/start`,
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          ...PG_CLIENT_HEADER,
        },
        body: JSON.stringify({con, validity: secondsTill4AM()}),
      },
      result: {
        url: (o, {sessionToken}) => `${o.url}/v2/request/jwt/${sessionToken}`,
        headers: PG_CLIENT_HEADER,
        parseResponse: r => r.text(),
      },
    }
  });
  return yivi_web;
}

export async function getKey(jwt, sort, timestamp, signingKeyRequest) {
  const url = `${PKG_URL}/v2/irma/${sort}${timestamp ? `/${timestamp.toString()}` : ''}`;
  return fetch(url, {
    method: sort === KEY_SORTS.Decryption ? 'GET' : 'POST',
    headers: {
      Authorization: `Bearer ${jwt}`,
      ...PG_CLIENT_HEADER,
      'content-type': 'application/json',
    },
    ...(signingKeyRequest && {
      body: JSON.stringify({...signingKeyRequest}),
    })
  })
  .then(r => r.json())
  .then(json => {
    if (json.status !== 'DONE' || json.proofStatus !== 'VALID') {
      throw new MvError('Not done and valid');
    }
    return sort === KEY_SORTS.Decryption
      ? json.key
      : {pubSignKey: json.pubSignKey, privSignKey: json.privSignKey};
  });
}

function secondsTill4AM() {
  const now = Date.now();
  const nextMidnight = new Date(now).setHours(24, 0, 0, 0);
  const secondsTillMidnight = Math.round((nextMidnight - now) / 1000);
  const secondsTill4AM = secondsTillMidnight + 4 * 60 * 60;
  return secondsTill4AM % (24 * 60 * 60);
}

export async function hashCon(con) {
  const sorted = con.sort(
    (att1, att2) =>
      att1.t.localeCompare(att2.t) || att1.v.localeCompare(att2.v)
  );
  const hash = await hashString(JSON.stringify(sorted));
  return hash;
}

async function hashString(message) {
  const msgArray = new TextEncoder().encode(message);
  const hashBuffer = await crypto.subtle.digest('SHA-256', msgArray);
  const hashArray = Array.from(new Uint8Array(hashBuffer));
  const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
  return hashHex;
}

// First tries to download the public key from the PKG.
// If this fails, it falls back to a public key in localStorage.
// The public key is stored iff there was no public key or it was different.
// If no public key is found, either through the PKG or localStorage, the promise rejects.
async function retrievePublicKey() {
  return fetch(`${PKG_URL}/v2/parameters`, {
    headers: PG_CLIENT_HEADER,
  })
  .then(resp =>
    resp.json()
    .then(async ({publicKey}) => publicKey)
  )
  .catch(e => {
    console.log(
      `[background]: failed to retrieve public key from PKG: ${e.toString()}`
    );
    throw new MvError('no public key');
  });
}

export async function encryptMessage({data, sealOptions}) {
  const pk_promise = retrievePublicKey();

  const [pk, mod] = await Promise.all([pk_promise, mod_promise]);
  const plainBytes = new TextEncoder().encode(data);

  // const ct = await mod.seal(pk, sealOptions, plainBytes);
  const readable = new ReadableStream({
    start(controller) {
      controller.enqueue(plainBytes);
      controller.close();
    }
  });
  let encrypted = new Uint8Array(0);
  const writable = new WritableStream({
    write(chunk) {
      encrypted = new Uint8Array([...encrypted, ...chunk]);
    }
  });

  await mod.sealStream(pk, sealOptions, readable, writable);
  return Array.from(encrypted);
}

export async function checkHeader({recipientId, pgFile}) {
  const vk_promise = fetch(`${PKG_URL}/v2/sign/parameters`)
  .then(r => r.json())
  .then(j => j.publicKey);

  const [vk, mod] = await Promise.all([vk_promise, mod_promise]);

  const armored = Uint8Array.from(pgFile);
  const encryptedFile = new Blob([armored]);
  const readable = encryptedFile.stream();
  const unsealer = await mod.StreamUnsealer.new(readable, vk);
  const recipients = unsealer.inspect_header();
  const me = recipients.get(recipientId);
  if (!me) {
    throw new MvError(l10n.map.yivi_recipient_unknown);
  }
  const keyRequest = {...me};
  let hints = me.con;

  // Convert hints.
  hints = hints.map(({t, v}) => {
    if (t === EMAIL_ATTRIBUTE_TYPE) {
      return {t, v: recipientId};
    } else {
      return {t, v};
    }
  });

  // Convert hidden policy to attribute request.
  keyRequest.con = keyRequest.con.map(({t, v}) => {
    if (t === EMAIL_ATTRIBUTE_TYPE) {
      return {t, v: recipientId};
    } else if (v === '' || v.includes('*')) {
      return {t};
    } else {
      return {t, v};
    }
  });
  return {unsealer, hints, keyRequest};
}

export async function decryptMessage({unsealer, usk, recipientId}) {
  let data = '';
  const decoder = new TextDecoder();
  const writable = new WritableStream({
    write: async chunk => {
      const decoded = decoder.decode(chunk, {stream: true});
      data += decoded;
    },
    close: async () => {
      const finalDecoded = decoder.decode();
      data += finalDecoded;
    },
  });
  const signatures = await unsealer.unseal(recipientId, usk, writable);
  return {data, signatures};
}

export function type_to_image(t) {
  let type;
  switch (t) {
    case 'pbdf.sidn-pbdf.email.email':
      type = 'envelope';
      break;
    case 'pbdf.sidn-pbdf.mobilenumber.mobilenumber':
      type = 'phone';
      break;
    case 'pbdf.pbdf.surfnet-2.id':
      type = 'education';
      break;
    case 'pbdf.nuts.agb.agbcode':
      type = 'health';
      break;
    case 'pbdf.gemeente.personalData.dateofbirth':
      type = 'calendar';
      break;
    default:
      type = 'personal';
      break;
  }
  return type;
}
