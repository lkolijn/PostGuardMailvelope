/**
 * Copyright (C) 2012-2017 Mailvelope GmbH
 * Licensed under the GNU Affero General Public License version 3
 */

import {hashCon} from './postguardModel';
import {jwtDecode} from 'jwt-decode';
import * as prefs from './prefs';

// yivi cache
let cache;
// caching active
let active;
// timeout in minutes
let timeout;
// max. number of operations per key
const RATE_LIMIT = 1000;
// time limit in minutes
const TIME_LIMIT = 1;
// max. nuber of operations in time limit
const TIME_LIMIT_RATE = 100;

export function init() {
  active = prefs.prefs.security.yivi_cache;
  timeout = prefs.prefs.security.yivi_timeout;
  cache = new Map();
  // register for updates
  prefs.addUpdateHandler(update);
}

function clearTimeouts() {
  // clear timeout functions
  cache.forEach(entry => clearTimeout(entry.timer));
}

function clearIntervals() {
  // clear interval functions
  cache.forEach(entry => clearInterval(entry.tlTimer));
}

function update(before, after) {
  if (!after.security) {
    return;
  }
  after.security.yivi_timeout ??= timeout;
  if (active != after.security.yivi_cache ||
      timeout != after.security.yivi_timeout) {
    // init cache
    clearTimeouts();
    clearIntervals();
    cache.clear();
    active = after.security.yivi_cache;
    timeout = after.security.yivi_timeout;
  }
}

/**
 * Get yivi jwt from cache
 */
export async function get(con) {
  const hashcon = await hashCon(con);
  if (cache.has(hashcon)) {
    const entry = cache.get(hashcon);
    const operations = 1;
    entry.operations -= operations;
    entry.tlOperations -= operations;
    if (!Math.max(0, entry.tlOperations)) {
      return;
    }
    const now = Date.now() / 1000;
    if (now > entry.exp) {
      // JWT has expired
      deleteEntry(hashCon);
    }
    if (Math.max(0, entry.operations)) {
      return entry.jwt;
    } else {
      // number of allowed operations exhausted
      deleteEntry(hashcon);
    }
  }
}

/**
 * Return true if key is cached
 * @param  {String} hashcon - primary key fingerprint
 * @return {Boolean} - true if cached
 */
export function isCached(hashcon) {
  return cache.has(hashcon);
}

/**
 * Delete key from cache
 * @param  {String} hashcon - primary key fingerprint
 */
function deleteEntry(hashcon) {
  clearTimeouts();
  clearIntervals();
  cache.delete(hashcon);
}

export {deleteEntry as delete};

/**
 * Set key and yivi in cache, start timeout
 * @param con - private key, expected unlocked
 * @param jwt
 * @param {Number}          [cacheTime] - timeout in minutes
 */
export async function set({con, jwt, cacheTime, reservedOperations = 0}) {
  const hash = await hashCon(con);
  const decoded = jwtDecode(jwt);
  let entry;
  if (cache.has(hash)) {
    entry = cache.get(hash);
    // update remaining number of operations
    entry.operations -= reservedOperations;
    clearInterval(entry.tlTimer);
  } else {
    entry = {jwt, exp: decoded.exp};
    // clear after timeout
    entry.timer = setTimeout(() => {
      deleteEntry(hash);
    }, (cacheTime || timeout) * 60 * 1000);
    // set max. number of operations
    entry.operations = Math.max(0, RATE_LIMIT - reservedOperations);
  }
  // clear after time limit rate has been reached
  entry.tlOperations =  Math.max(0, TIME_LIMIT_RATE - reservedOperations);
  entry.tlTimer = setInterval(() => {
    entry.tlOperations = TIME_LIMIT_RATE;
  }, TIME_LIMIT * 60 * 1000);
  cache.set(hash, entry);
}
