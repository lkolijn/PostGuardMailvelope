/**
 * Copyright (C) 2019 Mailvelope GmbH
 * Licensed under the GNU Affero General Public License version 3
 */

// import * as yivi from '@privacybydesign/yivi-frontend';
import React from 'react';
import PropTypes from 'prop-types';
import * as l10n from '../../lib/l10n';
import EventHandler from '../../lib/EventHandler';
import Spinner from '../util/Spinner';
import {createYiviWeb, getKey} from '../../modules/postguardModel';
import './yiviDialog.scss';
import {KEY_SORTS} from '../../lib/constants';

// register language strings
l10n.register([
  'yivi_heading',
  'yivi_heading_sign',
  'yivi_qr_prefix',
  'yivi_qr_prefix_sign',
  'yivi_help_header',
  'yivi_help_body',
  'yivi_help_link_text',
  'yivi_help_download_header',
  'yivi_dialog_title',
  'yivi_dialog_title_sign',
  'yivi_dialog_cache_identity',
  'yivi_heading',
  'yivi_heading_sign',
  'pbdf_sidn_pbdf_email_email',
  'pbdf_sidn_pbdf_mobilenumber_mobilenumber',
  'pbdf_gemeente_personalData_surname',
  'pbdf_gemeente_personalData_dateofbirth',
  'pbdf_nuts_agb_agbcode',
  'pbdf_pbdf_surfnet_2_id'
]);

export default class YiviDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      heading: '',
      title: '',
      qrPrefix: '',
      con: [],
      waiting: true,
      showError: false,
      cache: false
    };
    this.port = EventHandler.connect(`yiviDialog-${this.props.id}`, this);
    this.registerEventListeners();
    this.port.emit('yivi-dialog-init');
  }

  registerEventListeners() {
    this.port.on('set-init-data', this.setInitData);
  }

  setInitData({sort, heading, title, qrPrefix, con, hints, senderId, timestamp, cache}) {
    this.setState({
      sort,
      con,
      heading: heading !== '' ? l10n.map[heading.toLowerCase()] : '',
      title: title !== '' ? l10n.map[title.toLowerCase()] : '',
      qrPrefix: qrPrefix !== '' ? l10n.map[qrPrefix.toLowerCase()] : '',
      waiting: false,
      hints,
      senderId,
      timestamp,
      cache
    });
  }

  fillTable(table, data) {
    function row({t, v}) {
      const row = document.createElement('tr');
      const tdtype = document.createElement('td');
      const tdvalue = document.createElement('td');
      tdtype.innerText = l10n.map[t.replace(/\.|-/gi, '_').toLowerCase()] ?? t;
      tdvalue.innerText = v ? v : '';
      tdvalue.classList.add('value');
      row.appendChild(tdtype);
      row.appendChild(tdvalue);
      return row;
    }

    if (data.hints && table.rows.length == 0) {
      for (const {t, v} of data.hints) {
        table.appendChild(row({t, v}));
      }
    }
  }

  async componentDidUpdate() {
    const table = document.querySelector('table#attribute-table');
    if (table) {
      this.fillTable(table, this.state);
    }
    let yiviWeb;
    let key;
    let jwt;
    if (this.state.sort == KEY_SORTS.Signing) {
      const pubSignId = this.state.con;
      const privSignId = [];
      const keyrequest = {pubSignId, privSignId};
      const totalId = [...this.state.con, ...[]];
      yiviWeb = createYiviWeb(totalId, this.state.qrPrefix);
      jwt = await yiviWeb.start();
      key = await getKey(jwt, this.state.sort, undefined, keyrequest);
    } else {
      yiviWeb = createYiviWeb(this.state.con, this.state.qrPrefix);
      jwt = await yiviWeb.start();
      key = await getKey(jwt, this.state.sort, this.state.timestamp);
    }
    this.port.emit('yivi-dialog-ok', {jwt, key, cache: this.state.cache});
    console.log('Successful disclosure.');
  }

  onChangeCache(value) {
    this.setState({cache: value});
  }

  render() {
    return (
      <div className="modal d-block">
        <div className="modal-dialog h-100 mw-100 m-0">
          <div className="modal-content shadow-lg border-0 h-100" style={{backgroundColor: 'rgba(255,255,255,1)'}}>
            {this.state.waiting ? (
              <Spinner style={{margin: 'auto auto'}} />
            ) : (
              <>
                <div className="modal-header border-0 rounded-0 p-2 justify-content-left flex-shrink-0" style={{backgroundColor: 'rgba(2,46,61,1)'}}>
                  <img className="header-image" src="../../img/PostGuard/pg_logo_white.svg" alt="PostGuard logo" />
                </div>
                <div className="modal-body overflow-auto py-0 px-4">
                  <h2 className="pt-3 center">{this.state.title}</h2>
                  <h3 className="center" id="sender">{(this.state.sort === KEY_SORTS.Decryption) ? this.state.senderId : ''}</h3>
                  <h3 className="center">{this.state.heading}</h3>
                  <div id="attributes">
                    <table id="attribute-table"></table>
                  </div>
                  <div id="qr-container">
                    <img className="left" id="yivi-logo" src="../../img/PostGuard/yivi-logo.svg" />
                    <section className="yivi-web-center-child" id="yivi-web-form"></section>
                  </div>
                  <div className="center pt-2">
                    <div className="custom-control custom-checkbox">
                      <input type="checkbox" checked={this.state.cache} onChange={e => this.onChangeCache(e.target.checked)} className="custom-control-input" id="remember" />
                      <label className="custom-control-label" htmlFor="remember">{l10n.map.yivi_dialog_cache_identity}</label>
                    </div>
                  </div>
                  <div id="yivi-app">
                    <div className="yivi">
                      <div id="what-is-yivi">
                        <h3>{l10n.map.yivi_help_header}</h3>
                        <p>{l10n.map.yivi_help_body}</p>
                        <a href="https://yivi.app">{l10n.map.yivi_help_link_text}</a>
                      </div>
                      <div id="download-yivi">
                        <h3>{l10n.map.yivi_help_download_header}</h3>
                        <a href="https://play.google.com/store/apps/details?id=org.irmacard.cardemu">
                          <img alt="Get it on Google Play" src="../../img/PostGuard/google-play-badge.png" />
                        </a>
                        &nbsp;&nbsp;&nbsp;
                        <a href="https://apps.apple.com/us/app/irma-authentication/id1294092994">
                          <img src="../../img/PostGuard/appstore_badge.svg" alt="Download on the App Store" />
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    );
  }
}

YiviDialog.propTypes = {
  id: PropTypes.string,
};

