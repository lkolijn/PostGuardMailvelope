/**
 * Copyright (C) 2019 Mailvelope GmbH
 * Licensed under the GNU Affero General Public License version 3
 */

import React from 'react';
import ReactDOM from 'react-dom';
import * as l10n from '../../lib/l10n';
import YiviDialog from './yiviDialog';
import {addDocumentTitle} from '../../lib/util';

document.addEventListener('DOMContentLoaded', init);

l10n.register([
  'yivi_dialog_header'
]);

l10n.mapToLocal();

function init() {
  const query = new URLSearchParams(document.location.search);
  // component id
  const id = query.get('id') || '';
  addDocumentTitle(`Postguard - ${l10n.map.yivi_dialog_header}`);
  // component used as a container (client API)
  const root = document.createElement('div');
  ReactDOM.render(<YiviDialog id={id} />, document.body.appendChild(root));
}
